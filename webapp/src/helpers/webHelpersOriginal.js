#!/usr/bin/env node

"use strict";

import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import autobahn from 'autobahn';

const app = express();
const port = 3000;

app.use(cors()); // CORS
app.use(bodyParser.json());

const wsUri = "ws://localhost:18080/ws";
const realm = "com.leapsight.bondy";
const authmethods = ["anonymous"];

const createConnection = () => {
    return new autobahn.Connection({
        url: wsUri,
        realm: realm,
        authmethods: authmethods
    });
};

// Realms
    app.post('/create-realm', (req, res) => {
        const { uri, description, is_prototype, is_sso_realm, allow_connections, authmethods } = req.body;

        const connection = createConnection();

        connection.onopen = function (session) {
            console.log("Connected!");

            const newRealmConfig = {
                uri,
                description,
                is_prototype: Boolean(is_prototype),
                is_sso_realm: Boolean(is_sso_realm),
                allow_connections: Boolean(allow_connections),
                authmethods
            };

            session.call('bondy.realm.create', [newRealmConfig]).then(
                function (result) {
                    console.log('Success! Result:', result);
                    res.send({ success: true, result: result });
                },
                function (err) {
                    console.log('Error creating the realm...', err);
                    res.status(500).send({ success: false, error: err });
                }
            ).finally(() => {
                connection.close();
            });
        };

        connection.open();
    });

    app.get('/list-realms', (req, res) => {
        const connection = createConnection();

        connection.onopen = function (session) {
            console.log("Connected!");

            // CALL list to Realms
            session.call(
                'bondy.realm.list',
                []
            ).then(
                function (realmList) {
                    console.log('List of Realms: ', realmList);
                    res.json({ success: true, realms: realmList });
                    connection.close();
                },
                function (err) {
                    console.log('Failed to call procedure', err);
                    res.status(500).json({ success: false, error: err });
                    connection.close();
                }
            );
        };

        connection.open();
    });

    app.delete('/delete-realm/:realmUri', (req, res) => {
        const realmUri = req.params.realmUri;
        const force = req.query.force || false;

        const connection = createConnection();

        connection.onopen = function (session) {
            console.log("Connected!");

            // CALL delete Realm
            session.call(
                'bondy.realm.delete',
                [realmUri],
                { force: Boolean(force) }
            ).then(
                function (result) {
                    console.log('Realm deleted successfully:', result);
                    res.json({ success: true, result: result });
                    connection.close();
                },
                function (err) {
                    console.log('Failed to call procedure', err);
                    res.status(500).json({ success: false, error: err });
                    connection.close();
                }
            );
        };

        connection.open();
    });
// Realms

// Listen the port
app.listen(port, '0.0.0.0', () => {
    console.log(`Server is running at http://localhost:${port}`);
  });  
