import autobahn from "autobahn";
import nacl from "tweetnacl";

const url = "ws://localhost:18080/ws";
const realm = "com.leapsight.bondy";
const authMethods = ["anonymous"];

let connection = null; 

 export default {

    getAnonymousConnection() {
        if (!connection) {
            console.log("-----------------------");
            console.log(`Running AutobahnJS ${autobahn.version}`);
            console.log(`Wamp Router url='${url}' realm='${realm}'`);
            console.log("Trying to connect ...");

            connection = new autobahn.Connection({
                url: url,
                realm: realm,
                authmethods: authMethods
            });
        }
        return connection; 
    },
    
    getWampcraConnection(username, password) {
        console.log("-----------------------");
        console.log(`Running AutobahnJS ${autobahn.version}`);
        console.log(`Wamp Router url='${url}' realm='${realm}'`);
        console.log("Trying to connect ...");
        return new autobahn.Connection({
            url: url,
            realm: realm,
            authmethods: ["wampcra"],
            authid: username,
            onchallenge: function (session, method, extra) {
                if (method === "wampcra") {
                    console.log(`Authenticating using '${method}'`);
                    console.log(`On Challenge Called with extra ${JSON.stringify(extra)}`);
                    var key = autobahn.auth_cra.derive_key(
                        password, extra.salt, extra.iterations, extra.keylen);
                    return autobahn.auth_cra.sign(key, extra.challenge);
                }
            }
        });
    },

    getCryptosignConnection(username, pkey) {
        console.log(`Running AutobahnJS ${autobahn.version}`);
        console.log(`Wamp Router Url='${url}'`);
        console.log(`Wamp Realm='${realm}'`);
        console.log(`username='${username}'`);
        console.log(`pkey='${pkey}'`);
        console.log("Trying to connect ...");
        return new autobahn.Connection({
            url: url,
            realm: realm,
            authextra: {
                pubkey: pkey,
                trustroot: null,
                challenge: null,
                channel_binding: null
            },
            authmethods: ["cryptosign"],
            authid: username,
            onchallenge: function (session, method, extra) {
                console.log(`Authenticating using '${method}'`);
                console.log(`On Challenge Called with extra ${JSON.stringify(extra)}`);
                let appPrivkey = autobahn.util.htob(
                    "4ffddd896a530ce5ee8c86b83b0d31835490a97a9cd718cb2f09c9fd31c4a7d71766c9e6ec7d7b354fd7a2e4542753a23cae0b901228305621e5b8713299ccdd"
                );
                let challenge = autobahn.util.htob(extra.challenge);
                let signature = nacl.sign.detached(challenge, appPrivkey);
                return autobahn.util.btoh(signature);
            }
        });
    },

    // PENDING
    getPasswordConnection(username, password) {
            console.log("-----------------------");
            console.log(`Running AutobahnJS ${autobahn.version}`);
            console.log(`Wamp Router url='${url}' realm='${realm}'`);
            console.log("Trying to connect ...");
            return new autobahn.Connection({
                url: url,
                realm: realm,
                authmethods: ["password"],
                authid: username,
                onchallenge: function (session, method, extra) {
                    if (method === "password") {
                        console.log(`Authenticating using '${method}'`);
                        console.log(`On Challenge Called with extra ${JSON.stringify(extra)}`);
                        var key = autobahn.auth_cra.derive_key(
                            password, extra.salt, extra.iterations, extra.keylen);
                        return autobahn.auth_cra.sign(password, extra.challenge);
                    }
                }
            });
    },

    // Example
    /*
    openConnection(url, realm) {
        console.log("-----------------------");
        console.log(`Running AutobahnJS ${autobahn.version}`);
        console.log(`Wamp Router url='${url}' realm='${realm}'`);
        console.log("Trying to connect ...");
        let connection = new autobahn.Connection({
            url: url,
            realm: realm,
            authmethods:['wampcra'],
            authid: username,
            onchallenge: function (session, method, extra) {
                var key = autobahn.auth_cra.derive_key(
                    password, extra.salt, extra.iterations, extra.keylen);
                var signature = autobahn.auth_cra.sign(key, extra.challenge);
                return signature;
            }
         });
         connection.onopen = on_open;
         connection.onclose = on_close;
         connection.open();
    },
    */

    // List of realms
    /* async getRealms(session, setRealmsFun, setErrorFun) {
            await new Promise((resolve) => setTimeout(resolve, 2000));
            session.call("bondy.realm.list", [], {}, { timeout: 5000 })
            .then(
                function (res) {
                    setRealmsFun(res);
                },
                function (err) {
                    setErrorFun(err);
                }
            );
            setRealmsFun(realmList);
    },
    

    async getRealms(session) {
        try {
            const realmList = await session.call('bondy.realm.list', []);
            console.log('List of Realms:', realmList);
            return realmList;
        } catch (err) {
            console.error('Failed to call bondy.realm.list:', err);
            throw err; 
        }
    },

 
    // Create realm
    addRealm(formData, setErrorFun) {

        const wsUri = "ws://localhost:18080/ws";
        const realm = "com.leapsight.bondy";
        const authmethods = ["anonymous"];

            const connection = () => {
                return new autobahn.Connection({
                    url: wsUri,
                    realm: realm,
                    authmethods: authmethods
                }),

            connection.onopen = function (session) {
                console.log("Connected!");

                const { uri, description, is_prototype, is_sso_realm, allow_connections, authmethods } = formData;

                const newRealmConfig = {
                    uri,
                    description,
                    is_prototype: Boolean(is_prototype),
                    is_sso_realm: Boolean(is_sso_realm),
                    allow_connections: Boolean(allow_connections),
                    authmethods
                };
                session.call('bondy.realm.create', [newRealmConfig]).then(
                    function (result) {
                        console.log('Realm created successfully:', result);
                        session.send({ success: true, result: result });
                    },
                    function (err) {
                        console.error('Error creating the realm:', err);
                        session.status(500).send({ success: false, error: err });
                        setErrorFun(err);
                    }
                )
            }
        }
    }
    */
}