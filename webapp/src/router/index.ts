import { createRouter, createWebHistory } from 'vue-router'
import CreateRealm from '../components/CreateRealm.vue'
import Realms from '../components/Realms.vue'
import CreateGroup from '../components/CreateGroup.vue'
import Groups from '../components/Groups.vue'
import CreateUser from '../components/CreateUser.vue'
import Users from '../components/Users.vue'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/home/:username&:ifsessionexists?',
      name: 'home',
      component: Home,
      props: true 
    },
    {
      path: '/realms',
      name: 'realms',
      component: Realms
    },
    {
      path: '/create-realm',
      name: 'create-realm',
      component: CreateRealm
    },
    {
      path: '/create-group',
      name: 'create-group',
      component: CreateGroup
    },
    {
      path: '/groups',
      name: 'groups',
      component: Groups
    },
    {
      path: '/create-user',
      name: 'create-user',
      component: CreateUser
    },
    {
      path: '/users',
      name: 'users',
      component: Users
    }
  ]
})

export default router