# Project Setup

### Open terminal in the directory webapp. Execute for npm and VueJS
```sh
npm install
npm run serve 
```

### Open terminal in the directory webapp. Execute Bondy

```sh
docker run \
--rm \
-e BONDY_ERL_NODENAME=bondy1@127.0.0.1 \
-e BONDY_ERL_DISTRIBUTED_COOKIE=bondy \
-u 0:1000 \
-p 18080:18080 \
-p 18081:18081 \
-p 18082:18082 \
-p 18083:18083 \
-p 18084:18084 \
-p 18085:18085 \
-u 0:1000 \
-v "./bondy/etc:/bondy/etc" \
-v "./bondy/data:/bondy/data" \
--name bondy \
leapsight/bondy:1.0.0-rc.6
```


## Login
<!--### Method ANONYMOUS
```
Anonymous method. Just click on login
``` -->

### Method WAMPCRA
```
username: bondy
password: admin123456 
```

### Method CRYPTOSIGN
```
username: bondy
password: 1766c9e6ec7d7b354fd7a2e4542753a23cae0b901228305621e5b8713299ccdd 
```